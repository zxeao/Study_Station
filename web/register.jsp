<%--
  Created by IntelliJ IDEA.
  User: Zxeao~Lenovo
  Date: 2022/7/21
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="<%=basePath%>css/register.css" type="text/css">
</head>

<body>

</form>
<form action="<%=request.getContextPath()%>/registerAction" method="post" class="form">
    <div class="box">
        <h2>Register</h2>
        <div class="input-box">
            <label>账户</label>
            <input type="text" name="username" tabindex="1" required="required"/>
        </div>
        <div class="input-box">
            <label>昵称</label>
            <input type="text" name="name" tabindex="1" required="required"/>
        </div>
        <div class="input-box">
            <label>密码</label>
            <input type="password" name="password" tabindex="1" required="required"/>
        </div>
        <div class="input-box">
            <label>邮箱</label>
            <input type="email" name="email" tabindex="1" required="required"/>
        </div>
        <div class="btn-box">
            <div>
                <button class="submit" type="submit">注册</button>
                <button onclick="window.location.href='login.jsp'">返回</button>
            </div>
        </div>
    </div>>
</form>

<script type="text/javascript">
</script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: Zxeao~Lenovo
  Date: 2022/7/24
  Time: 13:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<script type="text/javascript" src="<%=basePath%>js/marked.js"> </script>
<script src="<%=request.getContextPath()%>/js/jquery-1.11.0.js"></script>

<div id="content">
    <h1>Test</h1>
</div>

<button type="button" onclick="loadXMLDoc()">修改内容</button>

<script>
    <%--let text = `${sessionScope.content}`;--%>

    function loadXMLDoc()
    {
        $.ajax({
            url:"http://localhost:8080/Study_Station_war_exploded/javaInfoController",//请求后台的路径
            type:"get",//请求后台的是 doGet方法
            data:{
                "number":1,
            },
            dataType:"json",// 请求后台数据返回的格式
            success:function (data) {// 回调函数，只要有数据返回，就执行该函数
                if (data.msg=="ok" ){
                    alert("Success")
                    document.getElementById('content').innerHTML =
                        marked.parse(data.content);
                    document.getElementById('content').innerHTML=data.content;
                }
                alert(data.msg)
            }

        })
    }
</script>

</body>
</html>

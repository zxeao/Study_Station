<%--
  Created by IntelliJ IDEA.
  User: Zxeao~Lenovo
  Date: 2022/7/21
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="<%=basePath%>css/login_style.css" type="text/css">
</head>

<body>

    </form>
        <form action="<%=request.getContextPath()%>/loginAction" method="post" class="form">
            <div class="box">
                <h2>Login</h2>
                <div class="input-box">
                    <label>账户</label>
                    <input type="text" name="username" tabindex="1" required="required"/>
                </div>
                <div class="input-box">
                    <label>密码</label>
                    <input type="password" name="password" tabindex="1" required="required"/>
                </div>
                <div class="btn-box">
                    <a href="#">忘记密码?</a>
                    <div>
                        <button class="submit" type="submit" >登录</button>
                        <button type="button" onclick="window.location.href='register.jsp'" id="register">注册</button>
                    </div>
                </div>
            </div>>
    </form>

    <script type="text/javascript">
    </script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: Chl
  Date: 2022/7/27
  Time: 23:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>个人信息中心</title>
    <link href="http://fonts.googleapis.com/css?family=Bitter" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="outer">
    <div id="header">
        <div id="logo">
            <h1>
                <a href="index.jsp">编程资料学习站</a>
            </h1>
        </div>
        <div id="nav">
            <ul>
                <li class="first">
                    <a href="#">————用户中心 &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                        &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                        &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</a>
                </li>
                <li>
                    <a href="about.jsp">关于我们</a>
                </li>
                <li class="last">
                    <a href="#">联系我们</a>
                </li>
            </ul>
            <br class="clear" />
        </div>
    </div>

    <form action="<%=request.getContextPath()%>/userController" method="get" class="form">
    <div id="main">
        <div id="content">
            <div class="input-content">
                <label>请输入新的昵称：</label>
                <input type="text" name="name" tabindex="1" required="required" />
            </div>

            <div class="input-content">
                <label>请输入新的密码：</label>
                <input type="password" name="password" tabindex="1" required="required">
            </div>
            <div class="input-content">
                <label>请输入新的邮箱：</label>
                <input type="email" name="email" tabindex="1" required="required"/>
                <input type="hidden" name="username" tabindex="1" required="required" value="${sessionScope.user.username}">
                <input type="hidden" name="purpose" tabindex="1" required="required" value="change">
            </div>
            <div>
            <button type="submit" class="aa">确定修改</button>
            <button type="reset" class="aa">重置</button>
        </div>
        </div>


        <div id="sidebar1">
            <h3>
                用户中心
            </h3>
            <ul class="linkedList">
                <li class="first">
                    <a href="usercenter.jsp">个人资料</a>
                </li>
                <li>
                    <a href="password.jsp">修改个人资料</a>
                </li>
                <li>
                    <a href="http://localhost:8080/Study_Station_war_exploded/userController?purpose=ranking">学习排行榜</a>
                </li>
                <li>
                    <a href="mylove.jsp">我的收藏（未完成）</a>
                </li>
                <li class="last">
                    <a href="#">背景设置</a>
                </li>
            </ul>
        </div>

        <br class="clear" />
    </div>
    </form>
</div>
</body>
</html>


<style>
    * {
        margin: 0;
        padding: 0;
    }

    a {
        color: #07768d;
    }

    a:hover {
        text-decoration: none;
    }

    body {
        font-size: 12pt;
        line-height: 2em;
        font-family: Georgia, serif;
        background: #56423e url('/img/bg2.jpg');
        color: #d5d0cf;
    }

    br.clear {
        clear: both;
    }

    h1,h2,h3,h4 {
        text-transform: lowercase;
    }

    h2,h3,h4 {
        margin-bottom: 1.25em;
        font-family: Bitter, serif;
        color: #fff;
    }

    img.left {
        float: left;
        margin: 5px 20px 20px 0;
    }

    img.top {
        margin: 5px 0 20px 0;
    }

    p {
        margin-bottom: 1.75em;
    }

    ul {
        margin-bottom: 1.75em;
    }

    .imageList {
        list-style: none;
    }

    .imageList li {
        clear: both;
    }

    #banner {
        background: #fff;
    }

    #box1 {
        width: 480px;
        overflow: hidden;
        margin: 0px 0 30px 0;
    }

    #box2 {
        width: 225px;
        overflow: hidden;
        float: left;
    }

    #box3 {
        width: 225px;
        overflow: hidden;
        margin: 0px 0 0 255px;
    }

    #content {
        width: 800px;
        height: 340px;
        font-size: 26px;
        float: right;
        padding: 35px;
        background: url("img/info.jpg") no-repeat;
    }

    #copyright {
        padding: 35px 0 65px 0;
        text-align: center;
        color: #896F6A;
    }

    #copyright a {
        color: #997F7A;
    }

    #header {
        position: relative;
        padding: 35px;
        height: 130px;
    }

    #logo {
        position: absolute;
        top: 35px;
        left: 35px;
        height: 130px;
        line-height: 130px;
    }

    #logo a {
        text-decoration: none;
        color: #fff;
    }

    #logo h1 {
        font-size: 3em;
        font-family: Bitter, serif;
        text-shadow: 0px 2px 1px #36221e;
    }

    #main {
        position: relative;
        padding: 0px;
        width: 1180px;
        color: #262626;
    }

    #main a {
        color: #141414;
    }

    #main h2, #main h3, #main h4 {
        color: #1a1a1a;
    }

    #main ul {
        list-style: none;
    }

    #main ul li {
        padding: 10px 0 10px 0;
        border-top: dotted 1px #d6d6d6;
    }

    #main ul li.first {
        padding-top: 0;
        border-top: 0;
    }

    #nav {
        position: absolute;
        right: 35px;
        height: 57px;
        line-height: 57px;
        top: 71px;
    }

    #nav a {
        text-decoration: none;
        text-transform: lowercase;
        color: #f1f0f0;
        text-shadow: 0px 2px 1px #36221e;
    }

    #nav li {
        margin: 0px 1em 0 1em;
    }

    #nav ul {
        list-style: none;
    }

    #nav ul li {
        float: left;
    }

    #outer {
        position: relative;
        width: 1180px;
        margin: 0px auto 0 auto;
    }

    #search input.button {
        margin-left: 1em;
        border: 0px;
        color: #fff;
        background: #07768d;
        padding: 6px;
    }

    #search input.text {
        border: dotted 1px #fff;
        padding: 5px;
    }

    #sidebar1 {
        width: 215px;
        float: left;
        padding: 35px;
        background: #b09792 url('/img/bg3.jpg');
        color: #ede9e8;
        border: solid 1px #C1B0AC;
        text-shadow: 0px 1px 1px #705752;
    }

    #sidebar1 a {
        color: #f9f7f6;
    }

    #sidebar1 h2, #sidebar1 h3, #sidebar1 h4 {
        color: #fff;
        text-shadow: 0px 1px 1px #402722;
    }

    #sidebar1 ul {
        list-style: none;
    }

    #sidebar1 ul li {
        padding: 10px 0 10px 0;
        border-top: dotted 1px #fde8e3;
    }

    #sidebar1 ul li.first {
        padding-top: 0;
        border-top: 0;
    }

    #sidebar2 {
        width: 215px;
        margin: 0px 35px 0 315px;
        padding: 35px;
        background: #886963 url('/img/bg2.jpg');
        color: #f3efef;
        border: solid 1px #967B76;
        text-shadow: 0px 1px 1px #583933;
    }

    #sidebar2 a {
        color: #f5f3f3;
    }

    #sidebar2 h2, #sidebar2 h3, #sidebar2 h4 {
        color: #fff;
        text-shadow: 0px 1px 1px #482923;
    }

    #sidebar2 ul {
        list-style: none;
    }

    #sidebar2 ul li {
        padding: 10px 0 10px 0;
        border-top: dotted 1px #cbb1ac;
    }

    #sidebar2 ul li.first {
        padding-top: 0;
        border-top: 0;
    }
    .aa {
        border: 2px;
        border: black;
        border-radius: 10px;
        padding: 5px;
        background-color: rgb(238, 223, 204);
    }
</style>
<%--
  Created by IntelliJ IDEA.
  User: Zxeao~Lenovo
  Date: 2022/7/21
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Untree.co">
    <%--  <link rel="shortcut icon" href="favicon.png">--%>

    <meta name="description" content=""/>
    <meta name="keywords" content=""/>


    <link href="<%=basePath%>https://fonts.googleapis.com/css?family=Inter:300,400,500,600, 700,900|Oswald:400,700"
          rel="stylesheet">


    <link rel="stylesheet" href="<%=basePath%>static/fonts/icomoon/style.css">

    <link rel="stylesheet" href="<%=basePath%>css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=basePath%>css/magnific-popup.css">
    <link rel="stylesheet" href="<%=basePath%>css/jquery-ui.css">
    <link rel="stylesheet" href="<%=basePath%>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<%=basePath%>css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<%=basePath%>static/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<%=basePath%>css/aos.css">
    <link rel="stylesheet" href="<%=basePath%>css/fancybox.min.css">

    <link rel="stylesheet" href="<%=basePath%>css/style.css">
    <link href="<%=basePath%>http://fonts.googleapis.com/css?family=Bitter" rel="stylesheet" type="text/css"/>
    <link href="<%=basePath%>style.css" rel="stylesheet" type="text/css"/>
    <title>基于java的编程学习资料网</title>
</head>
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="200">


<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="header-bar d-flex d-lg-block align-items-center site-navbar-target" data-aos="fade-right">
        <div class="site-logo">
            <a href="index.jsp">编程资料学习站</a>
        </div>

        <div class="d-inline-block d-lg-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#"
                                                                                                            class="site-menu-toggle js-menu-toggle "><span
                class="icon-menu h3"></span></a></div>

        <div class="main-menu">
            <ul class="js-clone-nav">
                <li>Java</li>
                <li>
                    <button type="button" onclick="loadXMLDoc(1)" class="content-menu">简介</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(2)" class="content-menu">版权声明</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(3)" class="content-menu">前言</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(4)" class="content-menu">一 对象的概念</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(5)" class="content-menu">二 Java的安装</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(6)" class="content-menu">三 万物皆对象</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(7)" class="content-menu">四 运算符</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(8)" class="content-menu">五 控制流</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(9)" class="content-menu">六 初始化和清理</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(10)" class="content-menu">七 封装</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(11)" class="content-menu">八 复用</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(12)" class="content-menu">九 多态</button>
                </li>
                <li>
                    <button type="button" onclick="loadXMLDoc(13)" class="content-menu">十 接口</button>
                </li>
            </ul>
            <ul class="social js-clone-nav">
                <li><a href="#"><img src="img/icon1.png" class="icons"></a></li>
                <li><a href="#"><img src="img/icon2.png" class="icons"></a></li>
                <li><a href="#"><img src="img/icon3.png" class="icons"></a></li>
                <li><a href="#"><img src="img/icon4.png" class="icons"></a></li>
                <li><a href="#"><img src="img/icon5.png" class="icons"></a></li>
            </ul>
        </div>
    </header>

    <div class="head">
        <input type="text" size="48" maxlength="48" value="" style="border:0">
        <div class="menu-icons">
            <a href="#">
                <img src="img/icon6.png" class="header-icon">
            </a>
            <a href="usercenter.jsp">
                <img src="img/icon8.png" class="header-icon">
            </a>

            <a href="http://localhost:8080/Study_Station_war_exploded/userController?purpose=ranking">
                <img src="img/icon7.png" class="header-icon">
            </a>
<%--            <ul id="dropdown" style="position: relative;">--%>
<%--                <li>--%>
<%--                    <dl class="displayNone">--%>
<%--                        <dd>5464</dd>--%>
<%--                        <dd>54sdwda</dd>--%>
<%--                        <dd>5464awetre</dd>--%>
<%--                        <dd>5464aas</dd>--%>
<%--                        <dd>5464fff</dd>--%>
<%--                    </dl>--%>
<%--                </li>--%>
<%--            </ul>--%>


        </div>
    </div>

    <div class="container" id="content">
        <div class="welcome">欢迎进入基于Java的编程资料学习网站</div>
        <img src="img/index.png">


    </div>


</div> <!-- .site-wrap -->

<script src="<%=basePath%>js/jquery-3.3.1.min.js"></script>
<script src="<%=basePath%>js/jquery-migrate-3.0.1.min.js"></script>
<script src="<%=basePath%>js/jquery.easing.1.3.js"></script>
<script src="<%=basePath%>js/jquery-ui.js"></script>
<script src="<%=basePath%>js/popper.min.js"></script>
<script src="<%=basePath%>js/bootstrap.min.js"></script>
<script src="<%=basePath%>js/owl.carousel.min.js"></script>
<script src="<%=basePath%>js/jquery.stellar.min.js"></script>
<script src="<%=basePath%>js/jquery.countdown.min.js"></script>
<script src="<%=basePath%>js/jquery.magnific-popup.min.js"></script>
<script src="<%=basePath%>js/aos.js"></script>
<script src="<%=basePath%>js/lozad.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-1.11.0.js"></script>
<script type="text/javascript" src="<%=basePath%>js/marked.js"></script>


<script src="<%=basePath%>js/jquery.fancybox.min.js"></script>

<script src="<%=basePath%>js/main.js"></script>
<script>

    function loadXMLDoc(number) {
        $.ajax({
            url: "http://localhost:8080/Study_Station_war_exploded/javaInfoController",//请求后台的路径
            type: "get",//请求后台的是 doGet方法
            data: {
                "number": number,
            },
            dataType: "json",// 请求后台数据返回的格式
            success: function (data) {// 回调函数，只要有数据返回，就执行该函数
                if (data.msg == "ok") {
                    // alert("Success")
                    document.getElementById('content').innerHTML =
                        marked.parse(data.content);
                }
            }

        })
    }
    // window.onload=function (){
    //     var dropdown =document.getElementById("dropdown");
    //     var dropdowbLi=dropdown.getElementsByTagName("li");
    //     dropdowbLi[0].onmouseover=function (){
    //         this.classList.add("show");
    //     }
    //     dropdowbLi[0].onmouseout=function (){
    //         this.classList.remove("show");
    //     }
    // }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
</body>
<style>
    .icons {
        width: 13px;
        height: 13px;
    }

    .header-icon {
        width: 30px;
        height: 30px;
    }

    .content-menu {
        border: 0;
        background-color: rgb(245, 238, 220);
    }

    .main-menu li button:active {
        border: 0;
        background-color: rgb(245, 238, 220);
    }

    .welcome {
        font-weight: bolder;
        font-size: 30px;
        margin-bottom: 15px;
        text-align: center;
        width: 1000px;
    }

    .container {
        padding-bottom: 15px;
    }

    #content img {
        width: 1050px;
    }
</style>
</html>

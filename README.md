# 项目简介
基于Java的编程资料学习站，不采用任何框架，使用Servlet原生进行开发  
项目开发资料主要参考菜鸟教程  
实现开发人员在该网站上的学习和资料查询  
初步目标为实现Java的资料查询与学习  
之后可能会扩展其他编程语言的资料  
团队成员皆为大二  
已经完成。

# 必读
1. 为了开发方便，数据库采用云服务器，所有人采用同一个数据库进行开发。
> 数据库地址：82.157.49.110  
> 数据库名称：Study_Station_Database  
> 数据库账户：******（已隐藏，有sql源码，自己搭建去）  
> 数据库密码：******
2. 请将自己的公钥部署到仓库  
- 仓库地址
> https://gitee.com/zxeao/Study_Station.git
- 教程
> https://blog.csdn.net/m0_46267097/article/details/107468428
3. 在将代码上传gitee时，切记不要删库，出现问题及时CSDN。
4. **不要划水，不要划水，不要划水。**

# Mysql版本
- mysql  Ver 14.14 Distrib 5.7.34, for Linux (x86_64)
# Maven版本
- apache-maven-3.8.4
# JDK版本
- java version "16.0.2" 2021-07-20

# 教程
- AJAX 教程
> https://www.runoob.com/ajax
- Servlet 教程
> https://www.runoob.com/servlet
- Git 教程
> https://www.runoob.com/git 
- JSP 教程
> https://www.runoob.com/jsp
- Maven 不用看，配置好了，直接用就行

# 注意事项
- 原生servlet所需要的依赖已经全部导入（问老师要的）
- 如果跑不起来
> 1.检查一下依赖对应的版本和自己电脑版本是否一致  
> 2.所有的环境变量，要根据网上的好好配，如JAVA_HOME,CLASSPATH(诸如此类）

# 开发进度
1. 数据库未创建
2. maven已配置
3. 仓库已创建
4. 项目已初始化
5. 搞了半天前端，本来是背景图片不显示，搞完之后css样式也失效了，搞了一晚上没搞好，哪位前端解决一下。
6. 成功修复前端图片不显示的bug，是浏览器缓存和jsp中的拦截，还有相对和绝对地址的锅，在web.xml放了通行证就ok了。
7. ![img.png](img.png)
8. html里面支持markdown的js，用npm安装在自己电脑生成，已经上传到项目![img_1.png](img_1.png)
9. 做了个demo，前端成功显示markdown，建立java_chapter数据库，在犹豫是把内容存进去还是用静态读取，建立java_实体类和Dao类，接口返回乱码。
10. 成功返回排名信息，接口里面还有用username里面，用法详情在apifox里面。![img_2.png](img_2.png)
11. over（中间过程懒得写）
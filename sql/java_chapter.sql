/*
 Navicat Premium Data Transfer

 Source Server         : 82.157.49.110_3306
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 82.157.49.110:3306
 Source Schema         : Study_Station_Database

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 29/07/2022 11:45:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for java_chapter
-- ----------------------------
DROP TABLE IF EXISTS `java_chapter`;
CREATE TABLE `java_chapter`  (
  `number` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chapter` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of java_chapter
-- ----------------------------
INSERT INTO `java_chapter` VALUES (1, '00-Introduction.md', 0);
INSERT INTO `java_chapter` VALUES (2, '00-On-Java-8.md', 0);
INSERT INTO `java_chapter` VALUES (3, '00-Preface.md', 0);
INSERT INTO `java_chapter` VALUES (4, '01-What-is-an-Object.md', 1);
INSERT INTO `java_chapter` VALUES (5, '02-Installing-Java-and-the-Book-Examples.md', 2);
INSERT INTO `java_chapter` VALUES (6, '03-Objects-Everywhere.md', 3);
INSERT INTO `java_chapter` VALUES (7, '04-Operators.md', 4);
INSERT INTO `java_chapter` VALUES (8, '05-Control-Flow.md', 5);
INSERT INTO `java_chapter` VALUES (9, '06-Housekeeping.md', 6);
INSERT INTO `java_chapter` VALUES (10, '07-Implementation-Hiding.md', 7);
INSERT INTO `java_chapter` VALUES (11, '08-Reuse.md', 8);
INSERT INTO `java_chapter` VALUES (12, '09-Polymorphism.md', 9);
INSERT INTO `java_chapter` VALUES (13, '10-Interfaces.md', 10);

SET FOREIGN_KEY_CHECKS = 1;

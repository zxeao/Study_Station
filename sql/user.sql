/*
 Navicat Premium Data Transfer

 Source Server         : 82.157.49.110_3306
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 82.157.49.110:3306
 Source Schema         : Study_Station_Database

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 29/07/2022 11:45:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `score` int(255) NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'fff', '氖B8牴#�\r蘌歰u剾', '1', 0);
INSERT INTO `user` VALUES ('2', '2', '�r崫L/co壧�,', '2', 8);
INSERT INTO `user` VALUES ('20202501492', 'éè¹æ', '�\n�9I篩V郬��>', '1053434868@qq.com', 0);
INSERT INTO `user` VALUES ('3', '3', '焖葉K\\恂(0徺颛后', '3', 6);
INSERT INTO `user` VALUES ('4', '4', '�鰕Ⅲ�憗uB,', '4', 0);
INSERT INTO `user` VALUES ('7', '7', '�鋉侮zZ6掭K�%C', '7@qq.com', 0);

SET FOREIGN_KEY_CHECKS = 1;

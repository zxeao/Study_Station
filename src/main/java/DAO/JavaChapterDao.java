package DAO;

import entity.JavaChapter;
import utils.DBHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JavaChapterDao {

    DBHelper dbHelper = new DBHelper();

    public JavaChapter queryByNumber(int number) {

        Connection connection = dbHelper.getConnection();
        PreparedStatement pst = null;

        try {
            pst = connection.prepareStatement("SELECT * FROM java_chapter WHERE number = ?");
            pst.setObject(1,number);

            ResultSet rs = pst.executeQuery();
            rs.next();

            String title = rs.getString("title");
            int chapter = rs.getInt("chapter");

            JavaChapter javaChapter = new JavaChapter(number,title,chapter);

            return javaChapter;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbHelper.close(connection,pst);
        }
        return null;
    }

}

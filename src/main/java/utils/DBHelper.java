package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBHelper {

    // 连接数据库的驱动  com.mysql.jdbc.Driver
    private static final String drive = "com.mysql.jdbc.Driver";
    // 连接数据库的url,这个端口号默认是3306,如果安装的时候没有改过，就写3306
    // info 的换成你们自己有的数据库
    private static final String  url = "jdbc:mysql://82.157.49.110:3306/Study_Station_Database?characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true";
    // 连接数据库的用户名和密码
    private  static final String username = "********";
    private static final String password = "********";
    // 需要连接数据库的类 选择的是 java.sql.Connection;
    public Connection conn = null;
    // 连接数据库的方法，将conn进行返回
    public Connection  getConnection()  {
        //模块化方式 注册驱动
        try {
            Class.forName(drive);
            //通过驱动管理器，获得连接
            conn = DriverManager.getConnection(url,username,password);
            System.out.println("数据库连接上了");
        }
        catch (Exception e){
            // 输出异常
          e.printStackTrace();
        }
        return  conn;
    }
    // 关闭数据库的方法
    public void close(Connection conn, PreparedStatement pst){
        // conn必须是非空值，才能够被关闭
        if (conn!=null){
            // crtl + Alt +t
            try {
                pst.close();
                conn.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public static void main(String[] args) {
        DBHelper dbHelper = new DBHelper();
        // 调用连接数据的方法
        dbHelper.getConnection();
    }
}

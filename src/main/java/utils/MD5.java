package utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// MD5方法，将密码等转换为md5值。
public class MD5 {
    public static String md5(String str) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] srcBytes = str.getBytes(StandardCharsets.UTF_8);
        md5.update(srcBytes);
        byte[] resultBytes = md5.digest();
        return new String(resultBytes);
    }
}

package Controller;

import DAO.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "loginAction", value = "/loginAction")
public class loginController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        super.doPost(req, resp);
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        System.out.println(username+"........"+password);

        UserDao userDao = new UserDao();
        User user = userDao.queryLogin(username,password);
        if (user != null) {
            request.getSession(). setAttribute("user",user);
            request.getRequestDispatcher("index.jsp").forward(request,response);
        }

    }
}

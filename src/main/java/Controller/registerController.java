package Controller;

import DAO.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "registerAction", value = "/registerAction")
public class registerController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        super.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        super.doPost(request, response);
        String username = request.getParameter("username");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        System.out.println("Register:"+username+"........"+password);

        UserDao userDao = new UserDao();
        User user = userDao.queryRegister(username,name,password,email,0);
        if (user != null) {
            request.getSession().setAttribute("user",user);
            request.getRequestDispatcher("login.jsp").forward(request,response);
        }
    }
}

package entity;

public class JavaChapter {
    private int number;
    private String title;
    private int chapter;

    public JavaChapter() {
    }

    public JavaChapter(int number, String title, int chapter) {
        this.number = number;
        this.title = title;
        this.chapter = chapter;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "JavaChapter{" +
                "number=" + number +
                ", title='" + title + '\'' +
                ", chapter=" + chapter +
                '}';
    }
}
